#!/bin/bash

../apache-maven-3.5.2/bin/mvn clean install
rm -r /var/lib/tomcat7/webapps/ROOT.war
rm -r /var/lib/tomcat7/webapps/ROOT
cp ~/nina_ricci_rep/target/nina_ricci-0.0.1.war /var/lib/tomcat7/webapps/ROOT.war
