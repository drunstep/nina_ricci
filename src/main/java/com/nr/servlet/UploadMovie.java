package com.nr.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.util.UUID;

/**
 * Servlet implementation class UploadMovie
 */
@WebServlet("/UploadMovie")
@MultipartConfig
public class UploadMovie extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public UploadMovie() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Part filePart = request.getPart("file");
        String locale = request.getParameter("locale");

        String path = "/usr/local/moodme/video/incoming/";

        String fileName = UUID.randomUUID().toString();

        OutputStream out = null;
        InputStream filecontent = null;

        try {
            out = new FileOutputStream(new File(path + File.separator
                    + fileName));
            filecontent = filePart.getInputStream();

            int read;
            final byte[] bytes = new byte[1024];

            while ((read = filecontent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
        } catch (FileNotFoundException fne) {
            throw new IOException(fne);
        } finally {
            if (out != null) {
                out.close();
            }
            if (filecontent != null) {
                filecontent.close();
            }
        }

	PrintWriter writer = response.getWriter();
	writer.println(locale + "/step2.html?video=" + fileName);

        //response.sendRedirect(locale + "/step2.html?video=" + fileName);
    }
}
