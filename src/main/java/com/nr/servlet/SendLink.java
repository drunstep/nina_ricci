package com.nr.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;

import com.nr.util.MailSender;

/**
 * Servlet implementation class SendLink
 */
@WebServlet("/SendLink")
@MultipartConfig
public class SendLink extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public SendLink() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String locale = request.getParameter("locale");
        String email = request.getParameter("email");
        String file = request.getParameter("url") + ".mp4";

        String subject = "";
        InputStream input;

        if (locale.equals("fr_FR")) {
            subject = "Les Monstres de Nina Ricci : ta vid\u00E9o";
            input = SaveData.class.getResourceAsStream("/video.html");
        } else if (locale.equals("en_CH")) {
            subject = "	Les Monstres de Nina Ricci : your video";
            input = SaveData.class.getResourceAsStream("/video_en.html");
        } else if (locale.equals("ru_RU")) {
            subject = "Les Monstres de Nina Ricci: твое сэлфи-видео";
            input = SaveData.class.getResourceAsStream("/video_ru.html");
        } else if (locale.equals("es_MX")) {
            subject = "Les Monstres de Nina Ricci : tu video";
            input = SaveData.class.getResourceAsStream("/video_mx.html");
        } else {
            input = SaveData.class.getResourceAsStream("/video.html");
        }

        String body = this.getString(input);

    	PrintWriter writer = response.getWriter();

    	try {
    	    MailSender.sendMailWithAttachment(email, subject, body, file);
    	    writer.println("true");
    	} catch(Exception e) {
                writer.println("false");
    	} finally {
    	    if (writer != null) {
    		writer.close();
    	    }
    	}

    }

   String getString(InputStream inputStream) throws IOException
   {
      ByteArrayOutputStream result = new ByteArrayOutputStream();
      byte[] buffer = new byte[1024];
      int length;
      while ((length = inputStream.read(buffer)) != -1) {
          result.write(buffer, 0, length);
      }
      // StandardCharsets.UTF_8.name() > JDK 7
      return result.toString("UTF-8");
   }
}
