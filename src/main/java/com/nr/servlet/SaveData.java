package com.nr.servlet;

import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.PreparedStatement;

import com.nr.util.MailSender;


@WebServlet("/SaveData")
@MultipartConfig
public class SaveData extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public SaveData() {
	super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String locale = request.getParameter("locale");

	String name = request.getParameter("name");
	String lastname = request.getParameter("lastname");
	String surname = request.getParameter("surname");
	String country = request.getParameter("countries");
	String email = request.getParameter("email");
	String agree = request.getParameter("agree");

        final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        final String DB_URL = "jdbc:mysql://localhost:3306/moodme";
        // ***********************************************************************
        // Database credentials
        final String USER = "moodme";
        final String PASS = "moodme";
        try {
		// Register JDBC driver
		Class.forName(JDBC_DRIVER);

		Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);

		PreparedStatement stmt = (PreparedStatement) conn.prepareStatement("insert into monsters_users (nation, name, surname, email, confirmed) values(?,?,?,?,?)");
		stmt.setString(1, country);
		stmt.setString(2, name);
		stmt.setString(3, lastname);
		stmt.setString(4, email);
		stmt.setBoolean(5, agree!=null);

		stmt.executeUpdate();

        InputStream input;
        String subject = "";
        if (locale.equals("fr_FR")) {
		    input = SaveData.class.getResourceAsStream("/newsletter.html");
            subject = "Les Monstres de Nina Ricci : Participation enregistr\u00E9e";
        } else if (locale.equals("en_CH")) {
            input = SaveData.class.getResourceAsStream("/newsletter_en.html");
            subject = "Les Monstres de Nina Ricci : Thank you for participating";
        } else if (locale.equals("ru_RU")) {
            input = SaveData.class.getResourceAsStream("/newsletter_ru.html");
            subject = "Les Monstres de Nina Ricci: Спасибо за участие";
        } else if (locale.equals("es_MX")) {
            input = SaveData.class.getResourceAsStream("/newsletter_mx.html");
            subject = "Les Monstres de Nina Ricci : Gracias por participar";
        } else {
            input = SaveData.class.getResourceAsStream("/newsletter.html");
        }
		String body = this.getString(input);
		MailSender.sendMail(email, subject, body);

	} catch (Exception e) {
            e.printStackTrace();
        }

	//response.sendRedirect(locale + "/index2.html");
	PrintWriter writer = response.getWriter();
	writer.println("true");
    }


   String getString(InputStream inputStream) throws IOException
   {
      ByteArrayOutputStream result = new ByteArrayOutputStream();
      byte[] buffer = new byte[1024];
      int length;
      while ((length = inputStream.read(buffer)) != -1) {
          result.write(buffer, 0, length);
      }
      // StandardCharsets.UTF_8.name() > JDK 7
      return result.toString("UTF-8");
   }

}
