package com.nr.util;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

 
public class MailSender {


    
   static public void sendMail( String email, String subj, String body ) throws Exception {

      // Sender's email ID needs to be mentioned
      String from = "contact-lesmonstres@ninaricci.com";
 
      // Assuming you are sending email from localhost
      String host = "mail.ninaricci.com";
 
      // Get system properties
      Properties properties = System.getProperties();
 
      // Setup mail server
      properties.setProperty("mail.smtp.host", host);
      properties.put("mail.smtp.auth", "true");
      properties.put("mail.smtp.starttls.enable", "false");

      Session session = Session.getInstance(properties,
            new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication("contact-lesmonstres@ninaricci.com", "cCb1m9!3sTb321#n");
                }
      });

      // Create a default MimeMessage object.
      MimeMessage message = new MimeMessage(session);
         
      // Set From: header field of the header.
      message.setFrom(new InternetAddress(from));
         
      // Set To: header field of the header.
      message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));

      // Set Subject: header field
      message.setSubject(subj, "utf-8");

      // Send the actual HTML message, as big as you like
      message.setContent(body, "text/html; charset=utf-8");
         
      // Send message
      Transport.send(message);
   }


   static public void sendMailWithAttachment( String email, String subj, String body, String filename ) throws Exception {

      // Sender's email ID needs to be mentioned
      String from = "contact-lesmonstres@ninaricci.com";
 
      // Assuming you are sending email from localhost
      String host = "mail.ninaricci.com";
 
      // Get system properties
      Properties properties = System.getProperties();
 
      // Setup mail server
      properties.setProperty("mail.smtp.host", host);
      properties.put("mail.smtp.auth", "true");
      properties.put("mail.smtp.starttls.enable", "false");

      Session session = Session.getInstance(properties,
            new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication("contact-lesmonstres@ninaricci.com", "cCb1m9!3sTb321#n");
                }
      });

      // Create a default MimeMessage object.
      MimeMessage message = new MimeMessage(session);
         
      // Set From: header field of the header.
      message.setFrom(new InternetAddress(from));
         
      // Set To: header field of the header.
      message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));

      // Set Subject: header field
      message.setSubject(subj, "utf-8");

      Multipart multipart = new MimeMultipart();

      MimeBodyPart htmlPart = new MimeBodyPart();
      htmlPart.setContent(body, "text/html");
      multipart.addBodyPart(htmlPart);

      String file = "/usr/local/moodme/video/outgoing/" + filename;
      String fileName = "lesmonstresdenina.mp4";
      DataSource source = new FileDataSource(file);
      MimeBodyPart messageBodyPart = new MimeBodyPart();
      messageBodyPart.setDataHandler(new DataHandler(source));
      messageBodyPart.setFileName(fileName);
      multipart.addBodyPart(messageBodyPart);

      message.setContent(multipart); 
         
      // Send message
      Transport.send(message);
   }

}
