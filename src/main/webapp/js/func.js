$(function(){




	$('#video').click( function(){

		if(!$(this).hasClass('play')){
			$('#video').get(0).play();
			$(this).addClass('play');
		}else{
			$('#video').get(0).pause();
			$(this).removeClass('play');
		}

	});



	if($('#countries').length){
		$("#countries").msDropdown({
			visibleRows: 5,
			rowHeight: 20
		});
	}


	if($('#modal-btn').length){
		setTimeout(function(){
			$("#modal-btn").fancybox().trigger('click');

		},5000);
	}

	$('#contact_form input').keyup(function(){
	    if ($('#contact_form').valid()){
    		$('#submit-btn').removeClass('btn-disabled');
	    } else{
	    	$('#submit-btn').addClass('btn-disabled');
	    }
	});

	$('#contact_form input[type="checkbox"]').change(function(){
	    if ($('#contact_form').valid()){
    		$('#submit-btn').removeClass('btn-disabled');
	    } else{
	    	$('#submit-btn').addClass('btn-disabled');
	    }
	});

	if($("#contact_form").length){
		$("#contact_form").validate({
		    errorPlacement: function(error, element) {
		      if (element.is(":checkbox") || element.is(":radio")) {
		        return false;
		      } else {
		        error.insertAfter(element);
		      }
		    },
			submitHandler: function(){

				var $form = $("#contact_form");
				var fd = new FormData(document.forms.contact_form);

				$.ajax({
					url: $form.attr('action'),
					data: fd,
					processData: false,
					contentType: false,
					type: 'POST',
					success: function(data){
						if ( !data ) {
							alert('Error')
						}
					}
				}).done(function(data){

				}).fail(function(){

				});

				$.fancybox.close();

			}
		});
  	}


	if($("#email_form").length){
		$("#email_form").validate({
			errorPlacement: function(error, element) {
				if (element.is(":checkbox") || element.is(":radio")) {
					return false;
				} else {
					error.insertAfter(element);
				}
			},
			submitHandler: function(){

				var $form = $("#email_form");
				var fd = new FormData(document.forms.email_form);

				$.ajax({
					url: $form.attr('action'),
					data: fd,
					processData: false,
					contentType: false,
					type: 'POST',
					success: function(data){
						if ( !data ) {
							alert('Error')
						}
					}
				}).done(function(data){

				}).fail(function(){

				});

				$.fancybox.close();

			}
		});
	}

	if($("#download_form").length){
		$("#download_form").validate({
			errorPlacement: function(error, element) {
				if (element.is(":checkbox") || element.is(":radio")) {
					return false;
				} else {
					error.insertAfter(element);
				}
			},
			submitHandler: function(){

				var $form = $("#download_form");
				var fd = new FormData(document.forms.download_form);

				$.ajax({
					url: $form.attr('action'),
					data: fd,
					processData: false,
					contentType: false,
					type: 'POST',
					success: function(data){
						if ( !data ) {
							alert('Error')
						}
					}
				}).done(function(data){

				}).fail(function(){

				});

				$.fancybox.close();
			}
		});
	}



	$('#email_form input').keyup(function(){
		if ($('#email_form').valid()){
			$('#submit-btn2').removeClass('btn-disabled');
		} else{
			$('#submit-btn2').addClass('btn-disabled');
		}
	});

	$('#email_form input[type="checkbox"]').change(function(){
		if ($('#email_form').valid()){
			$('#submit-btn2').removeClass('btn-disabled');
		} else{
			$('#submit-btn2').addClass('btn-disabled');
		}
	});

	$('#download_form input').keyup(function(){
		if ($('#download_form').valid()){
			$('#submit-btn3').removeClass('btn-disabled');
		} else{
			$('#submit-btn3').addClass('btn-disabled');
		}
	});

	$('#download_form input[type="checkbox"]').change(function(){
		if ($('#download_form').valid()){
			$('#submit-btn3').removeClass('btn-disabled');
		} else{
			$('#submit-btn3').addClass('btn-disabled');
		}
	});

	if($('#terms-btn').length){

		$("#terms-btn").fancybox();

	}



	$('.masks input:checkbox').click(function(){

		if($(this).attr("name") == 'group1'){
			$('input[name="group2"]:checkbox').prop('checked', false);
		}else if($(this).attr("name") == 'group2'){
			$('input[name="group1"]:checkbox').prop('checked', false);
		}

		if($(this).attr("name") == 'group3'){
			$('input[name="group3"]:checkbox').not(this).prop('checked', false);
		}
	});


	$('#agree').change(function(event) {
		if($(this).is(":checked")){
			$('#agree_success').removeClass('btn-disabled');
		}else{
			$('#agree_success').addClass('btn-disabled');
		}
	});



	$('.lang span').click(function(){
		$(this).parent().addClass('hover');
		$('body').append('<div class="lang-overlay"></div>');
	});

	$('.lang a').click(function(event){
		$('.lang span').text($(this).text());
		$('.lang').removeClass('hover');
		$('.lang-overlay').remove();
	});

	$('body').on('click', '.lang-overlay',function(){
		$('.lang').removeClass('hover');
		$('.lang-overlay').remove();
	});

	var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;


	if($('#inst-btn').length){

		$("#inst-btn").fancybox();

	}

	if (iOS) {
		$('body').addClass('iOS');

		if($('#download-btn').length){

			$("#download-btn").fancybox();
		}
	}

	$('.js-page-url').attr('value', location.search.split('video=')[1]);

});

$('#uploaded-file').on('change', function(){
	onUploadAjaxNew();
});

$('#btn-submit').on('click', function(){
	onUploadAjaxNew();
});



function onUploadAjaxNew() {
	var $form = $('#uploadForm');

	var $formContainer = $('.main-inner');

	$formContainer.addClass('form-loading');

	if (document.getElementById("uploaded-file").files.length == 0) {

		alertAboutFile();

		$formContainer.removeClass('form-loading');
	} else {
		setTimeout(function(){
			var fd = new FormData(document.forms.uploadForm);
			fd.append('file', $('#uploaded-file')[0].files[0] );

			$.ajax({
				url: $form.attr('action'),
				data: fd,
				processData: false,
				contentType: false,
				type: 'POST',
				success: function(data){
					console.log(data);
				}
			}).done(function(data){
				//location.href = '';
				$formContainer.removeClass('form-loading');

				window.location =  window.location.protocol + '//' + window.location.host + '/' + data + '';

				console.log( window.location =  window.location.protocol + '//' + window.location.host  + '/' + data + '' );

			}).fail(function(){
				$formContainer.removeClass('form-loading');
			});
		}, 1000);
	}
}

function onUpload() {
	var $form = $('#uploadForm');
	var $formContainer = $('.main-inner');

	$formContainer.addClass('form-loading');

	if (document.getElementById("uploaded-file").files.length == 0) {

		alertAboutFile();

		$formContainer.removeClass('form-loading');
	} else {
		setTimeout(function(){
			$form.submit();
		}, 1000);
	}
}

function alertAboutFile(){
	var locale = $('input[name=locale]').val();

	if ( locale == 'fr_FR' ) {
		alert("Prends ou télécharge d’abord une vidéo !");

	} else if ( locale == 'ru_RU' ) {
		alert("Cначала снимите или загрузите видео!");

	} else if ( locale == 'es_MX' ) {
		alert("¡Primero crea o sube un video!");

	} else {
		alert("First take or upload a video!");
	}
}


