var $videoScreen = $('.screen__video');
var $uploadScreen = $('.screen__upload');
var mediaRecorderSupport = false;

if ( typeof MediaRecorder != 'undefined'){
	mediaRecorderSupport = true;
}



var options = {mimeType: 'video/webm;codecs=vp9'};
var options2 = {mimeType: 'video/webm;codecs=vp9'};
var options3 = {mimeType: 'video/webm;codecs=vp9'};

if (!MediaRecorder.isTypeSupported(options.mimeType)) {
	console.log(options.mimeType + ' is not Supported');
} else {
	console.log('ok')
}

options2 = {mimeType: 'video/webm;codecs=vp8'};

if (!MediaRecorder.isTypeSupported(options2.mimeType)) {
	console.log(options2.mimeType + ' is not Supported');
} else {
	console.log('ok')
}


options3 = {mimeType: 'video/webm'};

if (!MediaRecorder.isTypeSupported(options3.mimeType)) {
	console.log(options3.mimeType + ' is not Supported');
} else {
	console.log('ok')
}


if ( $(window).width() > 1024 && mediaRecorderSupport) {

	/* globals MediaRecorder */

	var mediaSource = new MediaSource();
	mediaSource.addEventListener('sourceopen', handleSourceOpen, false);
	var mediaRecorder;
	var recordedBlobs;
	var sourceBuffer;

	var gumVideo = document.querySelector('video#gum');
	var recordedVideo = document.querySelector('video#recorded');

	var recordButton = document.querySelector('button#record');
	var playButton = document.querySelector('button#play');
	var downloadButton = document.querySelector('button#download');
	recordButton.onclick = toggleRecording;
	playButton.onclick = play;
	downloadButton.onclick = download;

// window.isSecureContext could be used for Chrome
	var isSecureOrigin = location.protocol === 'https:' ||
		location.hostname === 'localhost';
	if (!isSecureOrigin) {
		console.log('getUserMedia() must be run from a secure origin: HTTPS or localhost.' +
			'\n\nChanging protocol to HTTPS');
		//location.protocol = 'HTTPS';
	}

	var constraints = {
		audio: false,
		video: {
			width: { min: 480, ideal: 480, max: 960 },
			height: { min: 640, ideal: 640, max: 1280 }
		}
	};

	function handleSuccess(stream) {
		recordButton.disabled = false;
		console.log('getUserMedia() got stream: ', stream);
		window.stream = stream;
		gumVideo.srcObject = stream;

		//$uploadScreen.hide();
		$videoScreen.show();

	}

	function handleError(error) {
		console.log('navigator.getUserMedia error: ', error);
		$videoScreen.hide();
		//$uploadScreen.show();
	}

	navigator.mediaDevices.getUserMedia(constraints).
		then(handleSuccess).catch(handleError);

	function handleSourceOpen(event) {
		console.log('MediaSource opened');
		sourceBuffer = mediaSource.addSourceBuffer('video/webm; codecs="vp8"');
		console.log('Source buffer: ', sourceBuffer);
	}

	recordedVideo.addEventListener('error', function(ev) {
		console.error('MediaRecording.recordedMedia.error()');
		alert('Your browser can not play\n\n' + recordedVideo.src
			+ '\n\n media clip. event: ' + JSON.stringify(ev));
	}, true);

	function handleDataAvailable(event) {
		if (event.data && event.data.size > 0) {
			recordedBlobs.push(event.data);
		}
	}

	function handleStop(event) {
		console.log('Recorder stopped: ', event);
	}

	function toggleRecording() {
		if ( $('.js-video-btn').hasClass('video-btn_start') ) {
			startRecording();
		} else {
			stopRecording();
			$('.js-btn')
				.removeClass('video-btn_stop')
				.addClass('video-btn_start');
			playButton.disabled = false;
			downloadButton.disabled = false;
		}
	}

	function startRecording() {
		recordedBlobs = [];
		var options = {mimeType: 'video/webm;codecs=vp9'};
		if (!MediaRecorder.isTypeSupported(options.mimeType)) {
			console.log(options.mimeType + ' is not Supported');
			options = {mimeType: 'video/webm;codecs=vp8'};
			$videoScreen.hide();
			//$uploadScreen.show();

			if (!MediaRecorder.isTypeSupported(options.mimeType)) {
				console.log(options.mimeType + ' is not Supported');
				options = {mimeType: 'video/webm'};
				$videoScreen.hide();
				//$uploadScreen.show();

				if (!MediaRecorder.isTypeSupported(options.mimeType)) {
					console.log(options.mimeType + ' is not Supported');
					options = {mimeType: ''};

					$videoScreen.hide();
					//$uploadScreen.show();
				}
			}
		}
		try {
			mediaRecorder = new MediaRecorder(window.stream, options);
		} catch (e) {
			console.error('Exception while creating MediaRecorder: ' + e);
			console.log('Exception while creating MediaRecorder: '
				+ e + '. mimeType: ' + options.mimeType);

			$videoScreen.hide();
			$uploadScreen.show();

			return;
		}
		console.log('Created MediaRecorder', mediaRecorder, 'with options', options);
		$('.js-video-btn')
			.removeClass('video-btn_start')
			.addClass('video-btn_stop');
		playButton.disabled = true;
		downloadButton.disabled = true;
		mediaRecorder.onstop = handleStop;
		mediaRecorder.ondataavailable = handleDataAvailable;
		mediaRecorder.start(10); // collect 10ms of data
		console.log('MediaRecorder started', mediaRecorder);

		var timerVal = 5;
		videoTimer = setInterval(function(){

			timerVal--;
			$('#timer').text(timerVal);
			if (timerVal < 1) {
				stopRecording();
			}
		}, 1000);
	}

	function stopRecording() {
		mediaRecorder.stop();
		$('.js-video-btn')
			.removeClass('video-btn_stop')
			.addClass('video-btn_start');
		console.log('Recorded Blobs: ', recordedBlobs);
		//recordedVideo.controls = true;

		$('.video-wrapper').hide(0);
		$('.show-video').fadeIn(300);
		play();

		clearInterval(videoTimer);
		$('#timer').text('5');
	}

	function play() {
		var superBuffer = new Blob(recordedBlobs, {type: 'video/webm'});
		recordedVideo.src = window.URL.createObjectURL(superBuffer);
		// workaround for non-seekable video taken from
		// https://bugs.chromium.org/p/chromium/issues/detail?id=642012#c23
		recordedVideo.addEventListener('loadedmetadata', function() {
			if (recordedVideo.duration === Infinity) {
				recordedVideo.currentTime = 1e101;
				recordedVideo.ontimeupdate = function() {
					recordedVideo.currentTime = 0;
					recordedVideo.ontimeupdate = function() {
						delete recordedVideo.ontimeupdate;
						recordedVideo.play();
					};
				};
			}
		});
	}

	function download() {
		var blob = new Blob(recordedBlobs, {type: 'video/webm'});
		var url = window.URL.createObjectURL(blob);
		var a = document.createElement('a');
		a.style.display = 'none';
		a.href = url;
		a.download = 'test.webm';
		document.body.appendChild(a);
		a.click();
		setTimeout(function() {
			document.body.removeChild(a);
			window.URL.revokeObjectURL(url);
		}, 100);
	}

	$('.js-video-overwrite').on('click', function(){
		$('.show-video').hide(0);
		$('.video-wrapper').fadeIn(300);
	});

	$('.js-video-next').on('click', function(){

		var blob = new Blob(recordedBlobs, {type: 'video/webm'});

		onUploadAjaxVideo(blob);

	});

	function onUploadAjaxVideo(blob) {
		var $form = $('#uploadFormVideo');
		var $formContainer = $('.main-inner');

		$formContainer.addClass('form-loading-video');

		setTimeout(function(){
			var fd = new FormData(document.forms.uploadFormVideo);

			var fileOfBlob = new File([blob], 'video.webm');

			fd.append('file', fileOfBlob );

			//console.log(blob);
			console.log(fileOfBlob);

			$.ajax({
				url: $form.attr('action'),
				data: fd,
				processData: false,
				contentType: false,
				type: 'POST',
				success: function(data){
					console.log(data);
				}
			}).done(function(data){
				//location.href = '';
				$formContainer.removeClass('form-loading-video');
				window.location = 'https://lesmonstres.ninaricci.com/' + data;
			}).fail(function(){
				$formContainer.removeClass('form-loading-video');
			});
		}, 1000);
	}


}


